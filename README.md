# Expegit

Expegit is a command line tool that helps to organize git repositories to store experimental campaign results. It is written in rust, and makes use of [Large File Storage](https://git-lfs.github.com/) to avoid storing large binary files containing results, as diffs. In essence, it just wraps some git commands in an application to provide a comprehensive workflow for experimental campaigns. It is meant to be something similar to gitflow, for expermental campaign. 

What it allows to do:
+ Helps organize the repeated execution of an experiment under variations of parameters.
+ Helps store the results of those experiments. 
+ Helps keep track of the experiment version thanks to separation of experiment and campaign repositories. 
+ Helps several people work on the same experimental campaign.

What it does not allow to do:
+ Run experiments on clusters
+ Run analysis at campaign scale
+ Manage complex experimental workflows
+ Synchronize code between multiple workers of an experimental campaign

To install expegit, go to the [release page](https://gitlab.inria.fr/apere/expegit/wikis/releases).

## Quick start

We consider that you are writing the code of you experiment inside an other repository, which we will call the _experiment repository_. To initialize the campaign repository in a local and empty repository, simply run:
```bash
expegit init -p https://experiment-repository.git local-empty-campaign-repository  
```
That's it! By doing that, you have created a few folders in your local campaign repository:
``` 
local-campaign-repository/     # Your local campaign repository
│
├── xprp/                      # Your experiment repository as a git submodule, with some experimental code of yours
│   ├── experiment.sh
│   └── ...                    
├── excs/                      # An empty foler that will contain experiment executions
│   └── ...        
└── ...
```
The `-p` flag should have pushed your changes to the origin, so if you navigate to `https://campaign-repository.git`, you should see the same architecture.

Now, let's create a new experiment:

```bash
expegit exec new -p 7371ecc9232fd03ea0ecece7ab9db12171ef9d6f -- param_1 --param2="test" --param3 
``` 
Wuw, what does that means? We create a new _execution_ of the experiment, that will correspond to the experiment repository at commit `7371ecc9232fd03ea0ecece7ab9db12171ef9d6f`. Plus, we give a list of parameters `param_1 --param2="test" --param3`, that will be fed to your experiment script at execution time. By performing this, you have created a new folder in the `excs` directory:
```
campaign-repository/            
├── excs/                                    
│   └── 89cfd4a9-f42b-482a-9340-c5d762ea6f73  # The execution folder, which is given an identfier
│       ├── data/                             # The data folder where experimental data will be stored
│       │   └── lfs/                          # The special lfs that will use lfs storing for every data it contains
│       │       └── .gitattributes            
│       ├── experiment.sh                    #  Your experimental code at 7371ecc9232fd03ea0ecece7ab9db12171ef9d6f
│       └── ...
└── ...
```
As you see, your experimental script `experiment.sh` must be made so as to store experimental data directly in `data` for those not necessiting lfs and in `data/lfs` for those necessiting lfs.

You can now run your experiment on your own, by retrieving the parameters out of expegit:
```bash
cd campaign-repository/excs/89cfd4a9-f42b-482a-9340-c5d762ea6f73
./experiment.sh $(expegit exec get-params)
``` 

We only have to end up the experiment by running:
```bash
cd .. 
expegit finish -p 89cfd4a9-f42b-482a-9340-c5d762ea6f73
```

Your execution is now over, and you can repeat the process :)





