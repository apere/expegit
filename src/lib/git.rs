// This module contains functions to manipulate git repositories.
// All functions use processes.
//
// Author: Alexandre Péré
// Date 04/07/18

/////////////
// Imports //
/////////////
use std::path;
use std::process;
use std::str;
use super::{Error, GitErrorKind};

////////////////
// Public API //
////////////////
pub fn clone_remote_repo(git_url: &str, cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Cloning Remote Repository {} in {}", git_url, cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["clone", "--recurse-submodule", git_url])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result.
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to clone remote repository {} in {}", git_url, cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::RemoteClone(message)));
        },
    }
}

pub fn clone_local_repo(repo_path: &path::PathBuf, cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Cloning Local Repository {} to {}", repo_path.to_str().unwrap(), cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["clone","-ls", repo_path.to_str().unwrap(), "."])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result.
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to clone local repository {} in {}", repo_path.to_str().unwrap(), cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                                  String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::LocalClone(message)));

        },
    }
}

pub fn get_head(cmd_dir: &path::PathBuf) -> Result<String, Error>{
    // We log
    info!("Getting head in {}", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["rev-parse","HEAD"])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result.
    match git_output.status.success(){
        true=>Ok(str::replace(String::from_utf8(git_output.stdout).unwrap().as_str(), "\n", "")),
        false=> {
            warn!("Failed to retrieve head in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::UnknownError(message)));
        },
    }
}

pub fn checkout(commit_hash: &str, cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Performing hard reset in {} to commit {}", cmd_dir.to_str().unwrap(), commit_hash);
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["checkout", commit_hash])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result.
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to perform hard reset of {} to {}", cmd_dir.to_str().unwrap(), commit_hash);
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::UnknownError(message)));
        },
    }
}

pub fn get_origin_url(cmd_dir: &path::PathBuf) -> Result<String, Error>{
    // We log
    info!("Getting remote url in {}", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["remote", "get-url", "origin"])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result.
    match git_output.status.success(){
        true=>Ok(str::replace(String::from_utf8(git_output.stdout).unwrap().as_str(), "\n", "")),
        false=> {
            warn!("Failed to get origin url in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::RemoteConnection(message)));
        },
    }
}

pub fn add_submodule(sm_url: &str, name: &str, cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Adding Repository {} as a submodule {} in {}", sm_url, name, cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["submodule", "add", sm_url, name])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(()),
        false=>{
            warn!("Failed to add repository {} as a submodule {} in {}", sm_url, name, cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::AddSubmodule(message)));
        },
    }
}

pub fn get_staged_files(cmd_dir: &path::PathBuf) -> Result<Vec<path::PathBuf>, Error>{
    // We log
    info!("Getting staged files in {}", cmd_dir.to_str().unwrap());
    // We perform the git diff command
    let git_output = process::Command::new("git")
        .args(&["diff", "--name-only", "--cached"])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(String::from_utf8(git_output.stdout)
            .unwrap()
            .split("\n")
            .map(|x| cmd_dir.join(path::PathBuf::from(x))).collect()),
        false=> {
            warn!("Failed to get staged files in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::UnknownError(message)));
        },
    }
}

pub fn stage(stage_path: &path::PathBuf, cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Staging {} in {}", stage_path.to_str().unwrap(), cmd_dir.to_str().unwrap());
    // We perform the git add command
    let git_output = process::Command::new("git")
        .args(&["add", stage_path.to_str().unwrap()])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to stage {} in {}", stage_path.to_str().unwrap(), cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::Stage(message)));
        },
    }
}

pub fn commit(commmit_message: &str, cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Committing staged files in {}", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["commit", "-m", commmit_message])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to commit in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::Commit(message)));
        },
    }
}

pub fn push(cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log 
    info!("Pushing repository {} to remote", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["push"])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to push in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::RemotePush(message)));
        },
    }
}

pub fn pull(cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Pulling remote repository in {}", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["pull", "--no-edit"])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to pull in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::RemotePull(message)));
        },
    }
}

pub fn get_last_commit(branch: &str, cmd_dir: &path::PathBuf) -> Result<String, Error>{
    // We log
    info!("Getting last commit hash of repo:branch {}:{}", cmd_dir.to_str().unwrap(), branch);
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["log", "-n", "1", "--pretty=format:\"%H\"", branch])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(str::replace(String::from_utf8(git_output.stdout).unwrap().as_str(), "\"", "")),
        false=> {
            warn!("Failed to get last commit hash on branch {} in {}", branch, cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::UnknownError(message)));
        },
    }
}

pub fn get_all_commits(cmd_dir: &path::PathBuf) -> Result<Vec<String>, Error>{
    // We log
    info!("Getting all commit hashs of repo {}", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["log", "--pretty=format:\"%H\""])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(String::from_utf8(git_output.stdout)
            .unwrap()
            .split("\n")
            .map(|x| String::from(str::replace(x, "\"", "")))
            .collect()),
        false=> {
            warn!("Failed to get all commits hashs in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::UnknownError(message)));
        },
    }
}

pub fn clean(cmd_dir: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Cleaning in {}", cmd_dir.to_str().unwrap());
    // We perform the git command
    let git_output = process::Command::new("git")
        .args(&["clean", "-dxf"])
        .current_dir(cmd_dir)
        .stdin(process::Stdio::piped())
        .stdout(process::Stdio::piped())
        .stderr(process::Stdio::piped())
        .output()?;
    // Depending on the output, we return corresponding result
    match git_output.status.success(){
        true=>Ok(()),
        false=> {
            warn!("Failed to clean in {}", cmd_dir.to_str().unwrap());
            debug!("Command returned {:?}", git_output);
            let message = [String::from_utf8(git_output.stdout).unwrap(),
                              String::from_utf8(git_output.stderr).unwrap()].join("\n");
            return Err(Error::Git(GitErrorKind::UnknownError(message)));
        },
    }

}

#[cfg(test)]
mod test {
    /////////////
    // Imports //
    /////////////
    extern crate uuid;
    use std::fs;
    use std::collections::HashSet;
    use std::iter::FromIterator;
    use super::*;

    ///////////////
    // Constants //   Change the following constants to run the tests on your own.
    ///////////////
    // The path to tests folders
    static TEST_PATH: &str = "";
    // A repository containing a single file called "file.md"
    static SIMPLE_REPOSITORY_URL: &str = "";
    static SIMPLE_REPOSITORY_NAME: &str = "";
    static SIMPLE_REPOSITORY_HEAD: &str = "";
    static SIMPLE_REPOSITORY_COMMIT: &str = "";
    // Any repository that will be used to push and pull files (using hashes)
    static PUSH_PULL_REPOSITORY_URL: &str = "";
    static PUSH_PULL_REPOSITORY_NAME: &str = "";

    ////////////////
    // Unit-Tests //
    ////////////////
    #[test]
    fn test_clone_remote_repo(){
        let test_path = path::PathBuf::from(TEST_PATH).join("clone_remote_repo");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME));
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path)
            .expect("Failed to clone remote repository");
        assert!(test_path.join(SIMPLE_REPOSITORY_NAME).join("file.md").exists());
    }

    #[test]
    fn test_clone_local_repo(){
        let test_path = path::PathBuf::from(TEST_PATH).join("clone_local_repo");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path)
            .expect("Failed to clone remote repository");
        if test_path.join("local").exists() {
            fs::remove_dir_all(test_path.join("local"));
        }
        fs::create_dir(test_path.join("local")).expect("Failed to create directory");
        clone_local_repo(&test_path.join(SIMPLE_REPOSITORY_NAME), &test_path.join("local")).expect("Failed to clone local repository");
        assert!(test_path.join("local").join("file.md").exists());
    }

    #[test]
    fn test_get_head(){
        let test_path = path::PathBuf::from(TEST_PATH).join("get_head");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path)
            .expect("Failed to clone remote repository");
        let head = get_head(&test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get head");
        assert_eq!(head, SIMPLE_REPOSITORY_HEAD);
    }

    #[test]
    fn test_checkout(){
        let test_path = path::PathBuf::from(TEST_PATH).join("checkout");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists(){
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path)
            .expect("Failed to clone remote repository");
        let head = get_head(&test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get head");
        assert_eq!(head, SIMPLE_REPOSITORY_HEAD);
        checkout(SIMPLE_REPOSITORY_COMMIT, &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to reset to commit");
        let head = get_head(&test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get head after reset");
        assert_eq!(head, SIMPLE_REPOSITORY_COMMIT);
    }


    #[test]
    fn test_get_origin_url(){
        let test_path = path::PathBuf::from(TEST_PATH).join("get_origin_url");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        let url = get_origin_url(&test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get url");
        assert_eq!(url, SIMPLE_REPOSITORY_URL);
    }


    #[test]
    fn test_add_submodule(){
        let test_path = path::PathBuf::from(TEST_PATH).join("add_submodule");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        add_submodule(SIMPLE_REPOSITORY_URL, "xprp", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to add submodule");
        assert!(test_path.join(SIMPLE_REPOSITORY_NAME).join("xprp").join("file.md").exists());
    }

    #[test]
    fn test_stage_get_staged_files(){
        let test_path = path::PathBuf::from(TEST_PATH).join("stage_get_staged_files");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        let staged_files = get_staged_files(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert_eq!(staged_files.len(), 1);
        fs::File::create(test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")).expect("Failed to create file");
        stage(&path::PathBuf::from("."), &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to stage files");
        let staged_files = get_staged_files(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert_eq!(staged_files.len(), 2);
        assert!(staged_files.contains(&test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")));
    }


    #[test]
    fn test_git_commit() {
        let test_path = path::PathBuf::from(TEST_PATH).join("commit");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        fs::File::create(test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")).expect("Failed to create file");
        let before_head = get_head(&test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed get head");
        stage(&path::PathBuf::from("."), &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to track");
        let before_staged_files = get_staged_files(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        commit( "Test Commit", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to commit");
        let after_head = get_head(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        let after_staged_files = get_staged_files(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert_ne!(before_head, after_head);
        assert_ne!(before_staged_files, after_staged_files);
        assert!(before_staged_files.contains(&test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")));
        assert!(!after_staged_files.contains(&test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")));
    }

    #[test]
    fn test_git_push_pull(){
        let test_path = path::PathBuf::from(TEST_PATH).join("push_pull");
        if !test_path.exists(){
            fs::create_dir_all(&test_path).unwrap();
            fs::create_dir_all(&test_path.join("1")).unwrap();
            fs::create_dir_all(&test_path.join("2")).unwrap();
        }
        if test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        }
        if test_path.join("2").join(PUSH_PULL_REPOSITORY_NAME).exists(){
            fs::remove_dir_all(test_path.join("2").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(PUSH_PULL_REPOSITORY_URL, &test_path.join("1")).expect("Failed to clone remote repository");
        clone_remote_repo(PUSH_PULL_REPOSITORY_URL, &test_path.join("2")).expect("Failed to clone remote repository");
        let my_uuid = uuid::Uuid::new_v4();
        fs::File::create(test_path.join("1")
            .join(PUSH_PULL_REPOSITORY_NAME)
            .join(format!("{}", my_uuid))).expect("Failed to create file");
        stage(&path::PathBuf::from("."), &test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        let head_before_commit_1 = get_head(&test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        commit("Test Commit", &test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        let head_after_commit_1 = get_head(&test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        push(&test_path.join("1").join(PUSH_PULL_REPOSITORY_NAME)).expect("Failed to push");
        let head_before_pull_2 = get_head(&test_path.join("2").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        pull(&test_path.join("2").join(PUSH_PULL_REPOSITORY_NAME)).expect("Failed to pull");
        let head_after_pull_2 = get_head(&test_path.join("2").join(PUSH_PULL_REPOSITORY_NAME)).unwrap();
        assert_ne!(head_before_commit_1, head_after_commit_1);
        assert_eq!(head_before_commit_1, head_before_pull_2);
        assert_eq!(head_after_commit_1, head_after_pull_2);
    }

    #[test]
    fn test_get_last_commit(){
        let test_path =path::PathBuf::from(TEST_PATH).join("get_last_commit");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        fs::File::create(test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")).expect("Failed to create file");
        let before_last_commit = get_last_commit("master", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get comit before");
        stage(&path::PathBuf::from("."), &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to track");
        commit( "Test Commit", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to commit");
        let after_last_commit = get_last_commit("master", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get commit after");
        assert_ne!(before_last_commit, after_last_commit);
        checkout(before_last_commit.as_str(), &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("failed to checkout");
        let after_after_last_commit = get_last_commit("master", &test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert_eq!(after_last_commit, after_after_last_commit);
    }

    #[test]
    fn test_get_all_commits_hashs(){
        let test_path = path::PathBuf::from(TEST_PATH).join("get_all_commits");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        fs::File::create(test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")).expect("Failed to create file");
        let before_last_commit = get_last_commit("master", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get comit before");
        stage(&path::PathBuf::from("."), &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to track");
        commit( "Test Commit", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to commit");
        let after_last_commit = get_last_commit("master", &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("Failed to get commit after");
        let all_commits = get_all_commits(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert!(all_commits.contains(&before_last_commit));
        assert!(all_commits.contains(&after_last_commit));
        checkout(all_commits.get(0).unwrap(), &test_path.join(SIMPLE_REPOSITORY_NAME)).expect("failed to checkout");
        let after_after_last_commit = get_last_commit("master", &test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        let all_commits = get_all_commits(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert!(all_commits.contains(&before_last_commit));
        assert!(all_commits.contains(&after_last_commit));
        assert!(all_commits.contains(&after_after_last_commit));
    }

    #[test]
    fn test_clean(){
        let test_path = path::PathBuf::from(TEST_PATH).join("clean");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(SIMPLE_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        }
        clone_remote_repo(SIMPLE_REPOSITORY_URL, &test_path).expect("Failed to clone remote repository");
        fs::File::create(test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md")).expect("Failed to create file");
        assert!(test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md").exists());
        clean(&test_path.join(SIMPLE_REPOSITORY_NAME)).unwrap();
        assert!(!test_path.join(SIMPLE_REPOSITORY_NAME).join("touch.md").exists());
    }
}
