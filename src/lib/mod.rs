// This module contains the functions called by the command line interface.
//
// Author: Alexandre Péré

/////////////
// Modules //
/////////////
mod git;
pub mod misc;

/////////////
// Imports //
/////////////
#[macro_use]
extern crate serde_derive;
extern crate clap;
extern crate pretty_logger;
#[macro_use]
extern crate log;
extern crate regex;
extern crate serde_yaml;
extern crate serde;
extern crate uuid;
extern crate chrono;
use std::path;
use std::fs;
use std::error;
use std::io;
use std::fmt;
use std::str;

//////////////////////
// Global Variables //
//////////////////////

static XPRP_RPATH: &str = "xprp";         // folder containing the experiment repository as a submodule
static EXCS_RPATH: &str = "excs";         // folder containing the experiment executions
static EXCCONF_RPATH: &str = ".excconf";  // file containing the execution parameters.
static RNRS_RPATH: &str ="rnrs";          // folder containing the runners
static RNR_RPATH: &str = "run.sh";        // file containing runner global script.
static DATA_RPATH: &str= "data";          // folder containing the output data in an execution folder
static LFS_RPATH:  &str = "lfs";          // folder containing lfs managed data in a data folder
static EXPEGIT_RPATH: &str = ".expegit";  // file containing expegit configuration

////////////
// Errors //
////////////

#[derive(Debug)]
pub enum GitErrorKind{
    NotAValidRepo,
    DifferingRemote(String),
    RemoteConnection(String),
    RemoteClone(String),
    RemotePush(String),
    RemotePull(String),
    LocalClone(String),
    AddSubmodule(String),
    Stage(String),
    Commit(String),
    UnknownError(String),
}

impl fmt::Display for GitErrorKind{
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result{
        match *self {
            GitErrorKind::NotAValidRepo => write!(f, "Not a valid repository."),
            GitErrorKind::DifferingRemote(ref s) => write!(f, "Remote branch has different last commit than local: {}", s),
            GitErrorKind::RemoteConnection(ref s) => write!(f, "Failed to connect to remote: {}", s),
            GitErrorKind::RemoteClone(ref s) => write!(f, "Failed to clone the remote repository: {}", s),
            GitErrorKind::RemotePush(ref s) => write!(f, "Failed to push to remote repository: {}", s),
            GitErrorKind::RemotePull(ref s) => write!(f, "Failed to pull remote repository: {}", s),
            GitErrorKind::LocalClone(ref s) => write!(f, "Failed to clone the local repository: {}", s),
            GitErrorKind::AddSubmodule(ref s) => write!(f, "Failed to add submodule: {}", s),
            GitErrorKind::Stage(ref s) => write!(f, "Failed to track data: {}", s),
            GitErrorKind::Commit(ref s) => write!(f, "Failed to commit staged data: {}", s),
            GitErrorKind::UnknownError(ref s) => write!(f, "Unknown Error: {}", s),
        }
    }
}

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Git(GitErrorKind),
    Json(serde_yaml::Error),
    Regex(regex::Error),
    InvalidCommit(String),
    InvalidIdentifier(String),
    InvalidRepository,
    NotImplemented,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::Io(ref err) => write!(f, "Io Error: {}", err),
            Error::Git(ref err) => write!(f, "Git Error: {}", err),
            Error::Json(ref err) => write!(f, "Json Error: {}", err),
            Error::Regex(ref err) => write!(f, "Regex Error: {}", err),
            Error::NotImplemented => write!(f, "Not Implemented"),
            Error::InvalidCommit(ref com) => write!(f, "Commit {} not found", com),
            Error::InvalidIdentifier(ref id) => write!(f, "Execution {} not found", id),
            Error::InvalidRepository => write!(f, "Not an expegit repository"),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Io(ref err) => err.description(),
            Error::Git(_) => "Error occurred when performing git command",
            Error::Json(ref err) => err.description(),
            Error::Regex(ref err) => err.description(),
            Error::NotImplemented => "Feature not yet implemented",
            Error::InvalidCommit(_) => "Commit not found in experiment repo commits",
            Error::InvalidIdentifier(_) => "Execution not found",
            Error::InvalidRepository => "Not an expegit repository"
        }
    }
    fn cause(&self) -> Option<&error::Error>{
        match *self{
            Error::Io(ref err) => Some(err),
            Error::Git(_) => None,
            Error::Json(ref err) => Some(err),
            Error::Regex(ref err) => Some(err),
            Error::NotImplemented => None,
            Error::InvalidCommit(_) => None,
            Error::InvalidRepository => None,
            Error::InvalidIdentifier(_) => None,
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(err: serde_yaml::Error) -> Error{
        Error::Json(err)
    }
}

impl From<regex::Error> for Error {
    fn from(err: regex::Error) -> Error {
        Error::Regex(err)
    }
}

/////////////////////
// Data Structures //
/////////////////////

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct CampaignConfig{
    cmp_origin_url: String,
    xpr_origin_url: String,
    cmp_path: path::PathBuf,
    xprp_rpath: path::PathBuf,
    excs_rpath: path::PathBuf,
    rnrs_rpath: path::PathBuf,
    expegit_rpath: path::PathBuf,
    rnr_rpath: path::PathBuf,
}

impl CampaignConfig{
    pub fn new(local_path: &path::PathBuf, xprp_repo_url: &str) -> Result<CampaignConfig,Error>{
        if !local_path.exists(){
            panic!("Should provide path to an existing directory.")
        }
        let origin_url = match git::get_origin_url(&local_path){
            Err(err) => {
                error!("Failed to get origin url. Does that repository have a remote branch ?");
                return Err(err);
            },
            Ok(url) => url,
        };
        return Ok(CampaignConfig{
            cmp_origin_url: origin_url,
            xpr_origin_url: String::from(xprp_repo_url),
            cmp_path: local_path.clone(),
            xprp_rpath: path::PathBuf::from(XPRP_RPATH),
            excs_rpath: path::PathBuf::from(EXCS_RPATH),
            rnrs_rpath: path::PathBuf::from(RNRS_RPATH),
            expegit_rpath: path::PathBuf::from(EXPEGIT_RPATH),
            rnr_rpath: path::PathBuf::from(RNR_RPATH),
        });
    }
    pub fn get_campaign_origin_url(&self) -> &str{
        &self.cmp_origin_url
    }
    pub fn get_experiment_origin_url(&self) -> &str{
        &self.xpr_origin_url
    }
    pub fn get_campaign_path(&self) -> path::PathBuf{
        self.cmp_path.clone()
    }
    pub fn get_experiment_repo_path(&self) -> path::PathBuf{
        self.cmp_path.join(&self.xprp_rpath)
    }
    pub fn get_executions_path(&self) -> path::PathBuf{
        self.cmp_path.join(&self.excs_rpath)
    }
    pub fn get_runners_path(&self) -> path::PathBuf{
        self.cmp_path.join(&self.rnrs_rpath)
    }
    pub fn get_runner_script_path(&self) -> path::PathBuf {
        self.cmp_path.join(&self.rnrs_rpath).join(&self.rnr_rpath)
    }
    pub fn get_expegit_path(&self) -> path::PathBuf{
        self.cmp_path.join(&self.expegit_rpath)
    }
    pub fn import(cmp_path: &path::PathBuf) -> Result<CampaignConfig, Error>{
        let file = fs::File::open(cmp_path.join(EXPEGIT_RPATH))?;
        let mut config:CampaignConfig = serde_yaml::from_reader(file)?;
        config.cmp_path = fs::canonicalize(cmp_path)?;
        let config = config;
        return Ok(config);
    }
    pub fn export(&self) -> Result<(), Error>{
        let file = fs::File::create(self.get_expegit_path()).map_err(Error::Io)?;
        let mut config = self.clone();
        config.cmp_path = path::PathBuf::from("");
        serde_yaml::to_writer(file, &config).map_err(Error::Json)?;
        return Ok(());
    }
}

impl fmt::Display for CampaignConfig{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        let cmp_name = regex::Regex::new(r"([^/]*)\.git")
            .unwrap()
            .captures(self.get_campaign_origin_url())
            .unwrap()
            .get(1)
            .unwrap()
            .as_str();
        let xprp_name = regex::Regex::new(r"([^/]*)\.git")
            .unwrap()
            .captures(self.get_experiment_origin_url())
            .unwrap()
            .get(1)
            .unwrap()
            .as_str();
        write!(f, "Campaign<{}[{}]>", cmp_name, xprp_name)
    }
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub enum ExecutionState {
    Initialized,
    Finished,
    Removed,
}

impl<'a> From<&'a str> for ExecutionState {
    fn from(s: &str) -> ExecutionState {
        match s {
            "initialized" => ExecutionState::Initialized,
            "finished" => ExecutionState::Finished,
            "removed" => ExecutionState::Removed,
            _ => panic!("Unknown state string"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq, Clone)]
pub struct ExecutionConfig{
    commit: String,
    parameters: String,
    state: ExecutionState,
    reset_count: u32,
    excs_path: path::PathBuf,
    data_rpath: path::PathBuf,
    lfs_rpath: path::PathBuf,
    executor: Option<String>,
    execution_date: Option<String>,
    execution_duration: Option<u32>,
    generator: String,
    generation_date: String,
    identifier: String,
}

impl ExecutionConfig{
    pub fn new(excs_path: &path::PathBuf, commit: &str, parameters: &str) -> ExecutionConfig{
        if !excs_path.exists(){
            panic!("Should provide path to an existing directory.")
        }
        ExecutionConfig{
            commit: String::from(commit),
            parameters: String::from(parameters),
            state: ExecutionState::Initialized,
            reset_count: 0,
            excs_path: excs_path.clone(),
            data_rpath: path::PathBuf::from(DATA_RPATH),
            lfs_rpath: path::PathBuf::from(LFS_RPATH),
            executor: None,
            execution_date: None,
            execution_duration: None,
            generator: misc::get_hostname().unwrap(),
            generation_date: chrono::prelude::Utc::now().format("%Y-%m-%d %H:%M:%S").to_string(),
            identifier: format!("{}", uuid::Uuid::new_v4()),
        }
    }

    pub fn get_commit(&self) -> &str{
        return &self.commit
    }

    pub fn get_parameters(&self) -> &str{
        return &self.parameters
    }

    pub fn get_state(&self) -> ExecutionState{
        return self.state.clone()
    }

    pub fn set_state(&mut self, state: ExecutionState){
        self.state = state;
    }

    pub fn get_reset_count(&self) -> u32{
        return self.reset_count
    }

    pub fn increment_reset_count(&mut self){
        self.reset_count += 1;
    }

    pub fn get_path(&self) -> path::PathBuf{
        return self.excs_path.join(&self.identifier);
    }

    pub fn get_data_path(&self) -> path::PathBuf{
        return self.get_path().join(&self.data_rpath);
    }

    pub fn get_lfs_path(&self) -> path::PathBuf{
        return self.get_data_path().join(&self.lfs_rpath);
    }

    pub fn get_executor(&self) -> &Option<String>{
        return &self.executor;
    }

    pub fn set_executor(&mut self, executor: &str){
        self.executor = Some(String::from(executor));
    }

    pub fn reset_executor(&mut self){
        self.executor = None;
    }

    pub fn get_execution_date(&self) -> &Option<String>{
        return &self.execution_date;
    }

    pub fn set_execution_date(&mut self, date: &str){
        self.execution_date = Some(String::from(date));
    }

    pub fn reset_execution_date(&mut self){
        self.execution_date = None;
    }

    pub fn get_execution_duration(&self) -> &Option<u32>{
        return &self.execution_duration;
    }

    pub fn set_execution_duration(&mut self, duration: u32){
        self.execution_duration = Some(duration);
    }

    pub fn reset_execution_duration(&mut self){
        self.execution_duration = None;
    }

    pub fn get_generator(&self) -> &str{
        return &self.generator;
    }

    pub fn get_generation_date(&self) -> &str{
        return &self.generation_date;
    }

    pub fn get_identifier(&self) -> &str{
        return &self.identifier;
    }

    pub fn import(exc_path: &path::PathBuf) -> Result<ExecutionConfig, Error>{
        let file = fs::File::open(exc_path.join(EXCCONF_RPATH))?;
        let mut config:ExecutionConfig = serde_yaml::from_reader(file)?;
        config.excs_path = exc_path.parent().unwrap().to_path_buf().clone();
        return Ok(config);
    }

    pub fn export(&self) -> Result<(), Error>{
        let file_path = &self.get_path().join(EXCCONF_RPATH);
        let file = fs::File::create(file_path).map_err(Error::Io)?;
        let mut config = self.clone();
        config.excs_path = path::PathBuf::from("");
        serde_yaml::to_writer(file, &config).map_err(Error::Json)?;
        return Ok(());
    }
}

impl fmt::Display for ExecutionConfig{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        write!(f, "Execution<{}>", self.get_identifier())
    }
}

////////////////
// Public API //
////////////////

pub fn init_cmp_repo(local_cmp_path: &path::PathBuf, remote_xprp_repo_url: &str) -> Result<CampaignConfig, Error>{
    // We check that local_cmp_path is valid as a repository
    let is_empty: bool = fs::read_dir(&local_cmp_path)?.into_iter().count() == 0;
    if !(local_cmp_path.is_dir() && local_cmp_path.join(".git").is_dir()) | is_empty {
        return Err(Error::Git(GitErrorKind::NotAValidRepo));
    }
    // We generate the campaign config
    let cmp_config = CampaignConfig::new(&local_cmp_path, remote_xprp_repo_url)?;
    // We pull xprp repo as a submodule
    git::add_submodule(cmp_config.get_experiment_origin_url(),
                       XPRP_RPATH,
                       &cmp_config.get_campaign_path())?;
    // We create excs directory
    fs::create_dir(cmp_config.get_executions_path())?;
    // We create rnrs directory
    fs::create_dir(cmp_config.get_runners_path())?;
    // We create rnrs/run.sh
    fs::File::create(cmp_config.get_runner_script_path())?;
    // We generate .gitignore to not push something else than data
    misc::write_gitkeep(&cmp_config.get_executions_path())?;
    // We export the config
    cmp_config.export()?;
    // We track-commit-push the whole repo
    git::stage(&path::PathBuf::from("."), &cmp_config.get_campaign_path())?;
    git::commit("Initializes Campaign Repository",&cmp_config.get_campaign_path())?;
    // git::push(&cmp_config.get_campaign_path())?;
    // We return the configuration.
    Ok(cmp_config)
}

pub fn pull_cmp_repo(local_cmp_path: &path::PathBuf) -> Result<CampaignConfig, Error>{
    // We check that local_cmp_path is a valid git repository
    if !local_cmp_path.exists()
        && local_cmp_path.join(".git").is_dir()
        && local_cmp_path.join(".expegit").exists(){
        panic!("Should provide an existing expegit repository.")
    }
    // We import the campaign config
    let cmp_config = CampaignConfig::import(&local_cmp_path)?;
    // We pull the repo
    git::pull(&cmp_config.get_campaign_path())?;
    // We return the campaign configuration
    Ok(cmp_config)
}

pub fn clone_cmp_repo(cmp_url: &str, cmp_parent_path: &path::PathBuf) -> Result<CampaignConfig, Error>{
    // We check that cmp_parent_path exists
    if !cmp_parent_path.exists(){
        panic!("Should provide an existing folder")
    }
    // We clone the repo
    git::clone_remote_repo(cmp_url, cmp_parent_path)?;
    // We import the campaign config
    let repo_name = regex::Regex::new(r"([^/]*)\.git")?
        .captures(cmp_url)
        .unwrap()
        .get(1)
        .unwrap()
        .as_str();
    let cmp_config = CampaignConfig::import(&cmp_parent_path.join(repo_name))?;
    // We return the campaign configuration
    Ok(cmp_config)
}

pub fn push_cmp_repo(local_cmp_path: &path::PathBuf) -> Result<CampaignConfig, Error>{
    // We check that local_cmp_path is valid
    if !local_cmp_path.exists()
        && local_cmp_path.join(".git").is_dir()
        && local_cmp_path.join(".expegit").exists(){
        panic!("Should provide an existing expegit repository.")
    }
    // We import the campaign config
    let cmp_config = CampaignConfig::import(&local_cmp_path)?;
    // We try to push
    git::push(&cmp_config.get_campaign_path())?;
    // We return the campaign config
    return Ok(cmp_config);
}

pub fn add_exc(local_cmp_path: &path::PathBuf, commit: &str, parameters: &str) -> Result<ExecutionConfig, Error>{
    // We load the campaign config
    let cmp_config = CampaignConfig::import(local_cmp_path)?;
    // We check that commit number is valid
    let commits = git::get_all_commits(&cmp_config.get_experiment_repo_path())?;
    if !commits.contains(&commit.to_string()){
        return Err(Error::InvalidCommit(commit.to_string()));
    }
    // We generate an execution config
    let exc_config = ExecutionConfig::new(&cmp_config.get_executions_path(), commit, parameters);
    // We make the directory
    fs::create_dir(&exc_config.get_path())?;
    // We clone the xprp local repo with depth 1 and make a hard reset to commit
    git::clone_local_repo(&cmp_config.get_experiment_repo_path(),
                          &exc_config.get_path())?;
    git::checkout(commit, &exc_config.get_path())?;
    // We create data directories
    fs::create_dir(&exc_config.get_data_path())?;
    fs::create_dir(&exc_config.get_lfs_path())?;
    // We replace the .gitignore
    misc::write_exc_gitignore(&exc_config.get_path())?;
    // We happen the .gitattributes
    misc::write_lfs_gitattributes(&exc_config.get_lfs_path())?;
    // We save the execution config
    exc_config.export()?;
    // We remove the .git folder
    fs::remove_dir_all(exc_config.get_path().join(".git"))?;
    // We commit the folder
    git::stage(&path::PathBuf::from("."), &exc_config.get_path())?;
    git::commit(format!("Initialize execution {}", exc_config.get_identifier()).as_ref(),
               &cmp_config.get_campaign_path())?;
    // We return the execution config
    Ok(exc_config)
}

pub fn rm_exc(local_cmp_path: &path::PathBuf, identifier: &str) -> Result<ExecutionConfig, Error> {
    // We load the campaign_config
    let cmp_config = CampaignConfig::import(local_cmp_path)?;
    // We check that identifier is valid
    if !misc::get_all_identifiers(&cmp_config.get_executions_path())?.contains(&identifier.to_string()) {
        return Err(Error::InvalidIdentifier(String::from(identifier)));
    }
    // We load the execution config
    let mut exc_config = ExecutionConfig::import(&cmp_config.get_executions_path().join(identifier))?;
    // We clean up the folder
    git::clean(&exc_config.get_path())?;
    fs::remove_dir_all(&exc_config.get_data_path())?;
    // We modify the execution config
    exc_config.set_state(ExecutionState::Removed);
    exc_config.export()?;
    // We commit changes
    git::stage(&path::PathBuf::from("."), &exc_config.get_path())?;
    git::commit(format!("Remove execution {}", exc_config.get_identifier()).as_ref(),
               &cmp_config.get_campaign_path())?;
    // We return the execution config
    Ok(exc_config)
}

pub fn inform_exc(local_cmp_path: &path::PathBuf, identifier: &str, executor: Option<&str>, execution_date: Option<&str>, execution_duration: Option<u32>) -> Result<ExecutionConfig, Error>{
    // We load the campaign_config
    let cmp_config = CampaignConfig::import(local_cmp_path)?;
    // We check that identifier is valid
    if !misc::get_all_identifiers(&cmp_config.get_executions_path())?.contains(&identifier.to_string()) {
        return Err(Error::InvalidIdentifier(String::from(identifier)));
    }
    // We load the execution config
    let mut exc_config = ExecutionConfig::import(&cmp_config.get_executions_path().join(identifier))?;
    // We modify the config
    match executor{
        Some(ref value) => exc_config.set_executor(value),
        None => {},
    }
    match execution_date{
        Some(ref value) => exc_config.set_execution_date(value),
        None => {},
    }
    match execution_duration{
        Some(value) => exc_config.set_execution_duration(value),
        None => {},
    }
    // We export the config
    exc_config.export()?;
    // We commit the changes
    git::stage(&path::PathBuf::from("."), &exc_config.get_path())?;
    git::commit(format!("Informed execution {}", exc_config.get_identifier()).as_ref(),
               &cmp_config.get_campaign_path())?;
    // We return the execution config.
    return Ok(exc_config);
}

pub fn reset_exc(local_cmp_path: &path::PathBuf, identifier: &str) -> Result<ExecutionConfig, Error>{
    // We load the campaign_config
    let cmp_config = CampaignConfig::import(local_cmp_path)?;
    // We check that identifier is valid
    if !misc::get_all_identifiers(&cmp_config.get_executions_path())?.contains(&identifier.to_string()) {
        return Err(Error::InvalidIdentifier(String::from(identifier)));
    }
    // We load the execution config
    let mut exc_config = ExecutionConfig::import(&cmp_config.get_executions_path().join(identifier))?;
    // We remove the data directory
    fs::remove_dir_all(&exc_config.get_data_path())?;
    // We recreate the data directory
    fs::create_dir(&exc_config.get_data_path())?;
    fs::create_dir(&exc_config.get_lfs_path())?;
    misc::write_lfs_gitattributes(&exc_config.get_lfs_path())?;
    // We reset the execution config
    exc_config.reset_executor();
    exc_config.reset_execution_date();
    exc_config.reset_execution_duration();
    exc_config.increment_reset_count();
    exc_config.export()?;
    // We commit the changes
    git::stage(&path::PathBuf::from("."), &exc_config.get_path())?;
    git::commit(format!("Reset execution {}", exc_config.get_identifier()).as_ref(),
               &cmp_config.get_campaign_path())?;
    // We return the execution config.
    return Ok(exc_config);
}

pub fn finish_exc(local_cmp_path: &path::PathBuf, identifier: &str) -> Result<ExecutionConfig, Error>{
    // We load the campaign_config
    let cmp_config = CampaignConfig::import(local_cmp_path)?;
    // We check that identifier is valid
    if !misc::get_all_identifiers(&cmp_config.get_executions_path())?.contains(&identifier.to_string()) {
        return Err(Error::InvalidIdentifier(String::from(identifier)));
    }
    // We load the execution config
    let mut exc_config = ExecutionConfig::import(&cmp_config.get_executions_path().join(identifier))?;
    // We modify the execution config
    exc_config.set_state(ExecutionState::Finished);
    // We commit the changes
    git::stage(&path::PathBuf::from("."), &exc_config.get_path())?;
    git::commit(format!("Finishes execution {}", exc_config.get_identifier()).as_ref(),
               &cmp_config.get_campaign_path())?;
    // We clean up the folder
    git::clean(&exc_config.get_path())?;
    // We return the Execution config
    return Ok(exc_config);
}

#[cfg(test)]
mod test{
    /////////////
    // Imports //
    /////////////
    use std::env;
    use std::fs;
    use std::thread;
    use std::time;
    use std::collections::HashSet;
    use std::iter::FromIterator;
    use super::*;

    /////////////
    // Statics //
    /////////////
    // The path to tests folders
    static TEST_PATH: &str = "";
    // A repository containing the experimental code
    static EXPERIMENT_REPOSITORY_URL: &str = "";
    static EXPERIMENT_REPOSITORY_NAME: &str = "";
    static EXPERIMENT_REPOSITORY_HEAD: &str = "";
    static EXPERIMENT_REPOSITORY_COMMIT: &str = "";
    // A en empty repository that will be emptied to check campaign repo initialization (see tests)
    static INITIAL_CAMPAIGN_REPOSITORY_URL: &str = "";
    static INITIAL_CAMPAIGN_REPOSITORY_NAME: &str = "";
    // An initialized campaign repository with experiment EXPERIMENT_REPOSITORY_URL
    static CAMPAIGN_REPOSITORY_URL: &str = "";
    static CAMPAIGN_REPOSITORY_NAME: &str = "";

    ////////////////
    // Unit-tests //
    ////////////////
    #[test]
    fn test_campaign_config() {
        let test_path = path::PathBuf::from(TEST_PATH).join("campaign_config");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.exists() {
            fs::remove_dir_all(&test_path).unwrap();
        }
        fs::create_dir(&test_path);
        let config = CampaignConfig::new(&test_path, EXPERIMENT_REPOSITORY_URL).unwrap();
        assert_eq!(config.get_campaign_path(), test_path);
        assert_eq!(config.get_experiment_repo_path(), test_path.join(XPRP_RPATH));
        assert_eq!(config.get_runners_path(), test_path.join(RNRS_RPATH));
        assert_eq!(config.get_runner_script_path(), test_path.join(RNRS_RPATH).join(RNR_RPATH));
        assert_eq!(config.get_expegit_path(), test_path.join(EXPEGIT_RPATH));
        let config_1 = config.clone();
        if config.export().is_err() {
            panic!("Failed to export")
        }
        let config_2 = match CampaignConfig::import(&test_path) {
            Err(_) => panic!("Failed to import configuration"),
            Ok(cfg) => cfg,
        };
        assert_eq!(config_1, config_2);
    }

    #[test]
    fn test_execution_config() {
        let test_path = path::PathBuf::from(TEST_PATH).join("execution_config");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.exists() {
            fs::remove_dir_all(&test_path).unwrap();
        }
        fs::create_dir(&test_path);
        let config = ExecutionConfig::new(&test_path, "1235099959818991AQFD9", "-m -p --all 111");
        fs::create_dir(test_path.join(&config.identifier));
        config.export().unwrap();
        let config2 = ExecutionConfig::import(&test_path.join(&config.identifier)).unwrap();
        assert_eq!(config, config2);
        let mut config = config;
        assert_eq!(config.get_parameters(), "-m -p --all 111");
        assert_eq!(config.get_state(), ExecutionState::Initialized);
        config.set_state(ExecutionState::Finished);
        assert_eq!(config.get_state(), ExecutionState::Finished);
        assert_eq!(config.get_reset_count(), 0);
        config.increment_reset_count();
        assert_eq!(config.get_reset_count(), 1);
        assert_eq!(config.get_generator(), "apere@apere-XPS-13-9360");
        assert!(config.get_executor().is_none());
        config.set_executor("other@other_host");
        assert_eq!(config.get_executor(), &Some(String::from("other@other_host")));
        assert!(config.get_execution_date().is_none());
        config.set_execution_date("2018-06-28 13:36:bb");
        assert_eq!(config.get_execution_date(), &Some(String::from("2018-06-28 13:36:bb")));
        assert!(config.get_execution_duration().is_none());
        config.set_execution_duration(10000);
        assert_eq!(config.get_execution_duration(), &Some(10000));
    }

    ///////////////////////
    // Integration Tests //
    ///////////////////////
    #[test]
    fn test_init_cmp_repo() {
        let test_path = path::PathBuf::from(TEST_PATH).join("init_cmp_repo");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).exists() {
            for thing in test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).read_dir().unwrap(){
                let entry = thing.unwrap();
                if entry.path().is_file(){
                    fs::remove_file(entry.path());
                }
                    else if entry.path().file_name().unwrap()!=".git"{
                        fs::remove_dir_all(entry.path());
                    }
            }
            git::stage(&path::PathBuf::from("."), &test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME)).expect("Failed to stage changes");
            git::commit("Reset Repository", &test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME)).expect("Failed to commit changes");
            git::push(&test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME)).expect("Failed to push");
            git::pull(&test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME)).expect("Failed to pull");
            assert_eq!(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).read_dir().unwrap().collect::<Vec<_>>().len(), 1);
            fs::remove_dir_all(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME));
        }
        git::clone_remote_repo(INITIAL_CAMPAIGN_REPOSITORY_URL, &test_path).unwrap();
        let cmp_cfg = init_cmp_repo(&test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME), EXPERIMENT_REPOSITORY_URL).expect("Failed to initialize repository.");
        push_cmp_repo(&test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME)).expect("Failed to push");
        assert!(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).join(EXPEGIT_RPATH).exists());
        assert!(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).join(XPRP_RPATH).exists());
        assert!(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).join(RNRS_RPATH).exists());
        assert!(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).join(RNRS_RPATH).join(RNR_RPATH).exists());
        assert!(test_path.join(INITIAL_CAMPAIGN_REPOSITORY_NAME).join(EXCS_RPATH).exists());
    }

    #[test]
    fn test_add_exc() {
        let test_path = path::PathBuf::from(TEST_PATH).join("add_exc");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(CAMPAIGN_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(CAMPAIGN_REPOSITORY_NAME));
        }
        git::clone_remote_repo(CAMPAIGN_REPOSITORY_URL, &test_path).unwrap();
        let exc_cfg = add_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME), EXPERIMENT_REPOSITORY_HEAD, "--tests --no 1 arch").unwrap();
        assert_eq!(exc_cfg.get_state(), ExecutionState::Initialized);
        assert!(exc_cfg.get_path().exists());
        assert!(exc_cfg.get_path().join(".excconf").exists());
        assert!(exc_cfg.get_path().join(".gitignore").exists());
        assert!(exc_cfg.get_lfs_path().join(".gitattributes").exists());
    }

    #[test]
    fn test_rm_exc(){
        let test_path = path::PathBuf::from(TEST_PATH).join("rm_exc");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(CAMPAIGN_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(CAMPAIGN_REPOSITORY_NAME));
        }
        git::clone_remote_repo(CAMPAIGN_REPOSITORY_URL, &test_path).unwrap();
        let exc_cfg = add_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME), EXPERIMENT_REPOSITORY_HEAD, "--tests --no 1 arch").unwrap();
        let exc_cfg_2 = rm_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME),exc_cfg.get_identifier()).unwrap();
        assert_eq!(exc_cfg_2.get_state(), ExecutionState::Removed);
        assert!(exc_cfg_2.get_path().exists());
        assert!(exc_cfg_2.get_path().join(".excconf").exists());
        assert!(exc_cfg_2.get_path().join(".gitignore").exists());
        assert_eq!(exc_cfg_2.get_path().read_dir().unwrap().collect::<Vec<_>>().len(), 2);
    }

    #[test]
    fn test_inform_exc(){
        let test_path = path::PathBuf::from(TEST_PATH).join("inform_exc");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(CAMPAIGN_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(CAMPAIGN_REPOSITORY_NAME));
        }
        git::clone_remote_repo(CAMPAIGN_REPOSITORY_URL, &test_path).unwrap();
        let exc_cfg = add_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME), EXPERIMENT_REPOSITORY_HEAD, "--tests --no 1 arch").unwrap();
        let exc_cfg_2 = inform_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME),
                                   exc_cfg.get_identifier(),
                                   Some("executor@executorhost"),
                                   Some("2018-07-06 12:18:35"),
                                   Some(100000)).unwrap();
        assert_eq!(exc_cfg.get_path(), exc_cfg_2.get_path());
        assert_eq!(exc_cfg_2.get_executor(), &Some("executor@executorhost".to_owned()));
        assert_eq!(exc_cfg_2.get_execution_date(), &Some("2018-07-06 12:18:35".to_owned()));
        assert_eq!(exc_cfg_2.get_execution_duration(), &Some(100000));
    }

    #[test]
    fn test_reset_exc(){
        let test_path = path::PathBuf::from(TEST_PATH).join("reset_exc");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(CAMPAIGN_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(CAMPAIGN_REPOSITORY_NAME));
        }
        git::clone_remote_repo(CAMPAIGN_REPOSITORY_URL, &test_path).unwrap();
        let exc_cfg = add_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME), EXPERIMENT_REPOSITORY_HEAD, "--tests --no 1 arch").unwrap();
        fs::File::create(exc_cfg.get_data_path().join("touch.md")).expect("Failed to create file");
        let exc_cfg_2 = reset_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME),exc_cfg.get_identifier()).unwrap();
        assert_eq!(exc_cfg.get_path(), exc_cfg_2.get_path());
        assert_eq!(exc_cfg.get_reset_count(), 0);
        assert_eq!(exc_cfg_2.get_reset_count(), 1);
        assert!(!exc_cfg_2.get_data_path().join("touch.md").exists());
    }

    #[test]
    fn test_finish_exc(){
        let test_path = path::PathBuf::from(TEST_PATH).join("finish_exc");
        if !test_path.exists(){
            fs::create_dir_all(&test_path);
        }
        if test_path.join(CAMPAIGN_REPOSITORY_NAME).exists() {
            fs::remove_dir_all(test_path.join(CAMPAIGN_REPOSITORY_NAME));
        }
        git::clone_remote_repo(CAMPAIGN_REPOSITORY_URL, &test_path).unwrap();
        let exc_cfg = add_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME), EXPERIMENT_REPOSITORY_HEAD, "--tests --no 1 arch").unwrap();
        fs::File::create(exc_cfg.get_data_path().join("touch.md")).expect("Failed to create file");
        let exc_cfg_2 = finish_exc(&test_path.join(CAMPAIGN_REPOSITORY_NAME),exc_cfg.get_identifier()).unwrap();
        assert_eq!(exc_cfg.get_path(), exc_cfg_2.get_path());
        assert_eq!(exc_cfg.get_reset_count(), 0);
        assert_eq!(exc_cfg_2.get_state(), ExecutionState::Finished);
        assert!(exc_cfg_2.get_data_path().join("touch.md").exists());
        assert!(!exc_cfg_2.get_path().join("experiment.py").exists());
    }
}